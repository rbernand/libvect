# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/02/24 06:29:29 by rbernand          #+#    #+#              #
#    Updated: 2015/09/05 17:48:27 by rbernand         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=libvect.a
CC=	cc
FLAGS=-Wall -Wextra -Werror -O3 -ggdb
INCLUDES=include/
DIRLIB=lib/
DIROBJ=obj/
DIRSRC=src/
SRC=new_vect.c \
	sum.c \
	sub.c \
	prod.c \
	neg.c \
	add_float.c \
	get_norme_vect.c \
	normalize.c \
	str_vect.c \
	dot_product.c \
	cross_product.c \
	matrix_product.c \
	is_null.c \
	null.c \
	cosinus.c \
	rotate.c \
	rotate_neg.c
OBJ=$(SRC:%.c=$(DIROBJ)%.o)

all: init $(NAME)

init:
	@mkdir -p $(DIROBJ)
	@mkdir -p $(DIRLIB)

$(NAME): $(OBJ)
	@ranlib $(DIRLIB)$@
	@printf "\r\033[2K\033[1;36m%-20s\033[0;32m[ready]\033[0m\n" $(NAME)
	@ln -f $(DIRLIB)$@ $@

test: $(OBJ)
	gcc main.c -o test_vect $^ -I$(INCLUDES)
	./test_vect

$(DIROBJ)%.o: $(DIRSRC)%.c $(INCLUDES)
	@printf "\r\033[2KLinking $<"
	@$(CC) $(FLAGS) -o $@ -c $< -I$(INCLUDES)
	@ar -qc $(DIRLIB)$(NAME) $@

.PHONY: example
example: $(NAME)
	@printf "=== Compilation ===\n"
	gcc -o $@ example.c -L. -Iinclude -lvect
	@printf "===== Execution ====\n"
	./$@

clean:
	@rm -rf $(DIROBJ)

fclean: clean
	@rm -f $(NAME)
	@rm -rf $(DIRLIB)

re: fclean all
