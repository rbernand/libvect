/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cross_product.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 11:10:10 by rbernand          #+#    #+#             */
/*   Updated: 2015/06/04 11:58:55 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libvect.h>

t_vect3f			cross_product_v3f(t_vect3f v1, t_vect3f v2)
{
	t_vect3f				vout;

	vout.x = v1.y * v2.z + v1.z * v2.y;
	vout.y = v1.z * v2.x + v1.x * v2.z;
	vout.z = v1.x * v2.y + v1.y * v2.x;
	return (vout);
}
