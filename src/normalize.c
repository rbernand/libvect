/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normalize.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 10:36:48 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/03 13:09:09 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libvect.h>

t_vect3f		normalize_v3f(t_vect3f *v)
{
	double			norme;

	norme = get_norme_v3f(*v);
	if (norme)
	{
		v->x /= norme;
		v->y /= norme;
		v->z /= norme;
	}
	return (*v);
}
