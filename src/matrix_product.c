/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matric_product.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/01 11:03:42 by rbernand          #+#    #+#             */
/*   Updated: 2015/06/04 11:59:14 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libvect.h>

t_vect3f			matrix_product(t_vect3f mat[3], t_vect3f v)
{
	t_vect3f			vout;

	vout.x = v.x * mat[0].x + v.y * mat[1].x + v.z * mat[2].x;
	vout.y = v.x * mat[0].y + v.y * mat[1].y + v.z * mat[2].y;
	vout.z = v.x * mat[0].z + v.y * mat[1].z + v.z * mat[2].z;
	return (vout);
}
