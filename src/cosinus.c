/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cosinus.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbenand@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/05 16:33:49 by rbernand          #+#    #+#             */
/*   Updated: 2015/09/05 16:36:31 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <libvect.h>

float			cosinus_v3f(t_vect3f u, t_vect3f v)
{
	return (acos(dot_product_v3f(u, v)));
}
