/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/01 10:54:07 by rbernand          #+#    #+#             */
/*   Updated: 2015/09/02 16:42:41 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <libvect.h>

t_vect3f			rotate_x(t_vect3f v, float theta)
{
	t_vect3f			vout;
	t_vect3f			mat[3];

	mat[0].x = 1;
	mat[0].y = 0;
	mat[0].z = 0;
	mat[1].x = 0;
	mat[1].y = cos(theta);
	mat[1].z = sin(theta);
	mat[2].x = 0;
	mat[2].y = -sin(theta);
	mat[2].z = cos(theta);
	vout = matrix_product(mat, v);
	return (vout);
}

t_vect3f			rotate_y(t_vect3f v, float theta)
{
	t_vect3f			vout;
	t_vect3f			mat[3];

	mat[0].x = cos(theta);
	mat[0].y = 0;
	mat[0].z = -sin(theta);
	mat[1].x = 0;
	mat[1].y = 1;
	mat[1].z = 0;
	mat[2].x = sin(theta);
	mat[2].y = 0;
	mat[2].z = cos(theta);
	vout = matrix_product(mat, v);
	return (vout);
}

t_vect3f			rotate_z(t_vect3f v, float theta)
{
	t_vect3f			vout;
	t_vect3f			mat[3];

	mat[0].x = cos(theta);
	mat[0].y = sin(theta);
	mat[0].z = 0;
	mat[1].x = -sin(theta);
	mat[1].y = cos(theta);
	mat[1].z = 0;
	mat[2].x = 0;
	mat[2].y = 0;
	mat[2].z = 1;
	vout = matrix_product(mat, v);
	return (vout);
}

t_vect3f			rotate(t_vect3f v, t_vect3f rotation)
{
	t_vect3f			ret;

	ret = rotate_x(v, rotation.x);
	ret = rotate_y(ret, rotation.y);
	ret = rotate_z(ret, rotation.z);
	return (ret);
}
