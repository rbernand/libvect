/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate_neg.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbenand@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/05 17:44:57 by rbernand          #+#    #+#             */
/*   Updated: 2015/09/05 17:48:33 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libvect.h>

t_vect3f				rotate_neg(t_vect3f u, t_vect3f rot)
{
	t_vect3f			out;

	out = rotate_z(u, -rot.z);
	out = rotate_y(out, -rot.y);
	out = rotate_x(out, -rot.x);
	return (out);
}
