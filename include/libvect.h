/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libvect.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/26 15:20:19 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/05 11:38:54 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBVECT_H
# define LIBVECT_H

# define NULL_V3F			g_null_v3f

typedef struct s_vect3f		t_vect3f;

struct						s_vect3f
{
	float					x;
	float					y;
	float					z;
};

extern const t_vect3f		g_null_v3f;

int							is_null(t_vect3f *vect);
t_vect3f					new_v3f(float x, float y, float z);
t_vect3f					sum_v3f(t_vect3f v1, t_vect3f v2);
t_vect3f					sub_v3f(t_vect3f v1, t_vect3f v2);
t_vect3f					prod_v3f(t_vect3f v1, float n);
t_vect3f					add_float_v3f(t_vect3f v, float n);
t_vect3f					normalize_v3f(t_vect3f *v);
float						get_norme_v3f(t_vect3f v);
float						dot_product_v3f(t_vect3f v1, t_vect3f v2);
t_vect3f					cross_product_v3f(t_vect3f v1, t_vect3f v2);
t_vect3f					matrix_product(t_vect3f mat[3], t_vect3f v);
char						*str_v3f(t_vect3f v);
t_vect3f					rotate_x(t_vect3f v, float theta);
t_vect3f					rotate_y(t_vect3f v, float theta);
t_vect3f					rotate_z(t_vect3f v, float theta);
t_vect3f					rotate(t_vect3f v, t_vect3f rot);
t_vect3f					rotate_neg(t_vect3f v, t_vect3f rot);
float						cosinus_v3f(t_vect3f u, t_vect3f v);
t_vect3f					neg_v3f(t_vect3f v);

#endif
